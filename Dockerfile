# base image
FROM node:10

# set working directory
WORKDIR /app

# prepare the env (npm will not run script if using root)
ENV PATH=$PATH:/app/node_modules/.bin

# install and cache app dependencies
COPY package* ./

RUN npm ci --unsafe-perm

# add app
COPY . .

# start app
CMD npm run start
