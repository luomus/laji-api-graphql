# Dev environment
Install deps:
`npm i`

Start up a local redis database using the official redis docker image:
`docker run --name laji-graphql-redis -p 6379:6379 -d redis`

Launch the dev server:
`npm start`

GraphQL can now be tested at:
`localhost:3000/v0/graphql`

Remember to add a valid lajiapi access token to HTTP headers:
```{
	"authorization": <token>
}```
