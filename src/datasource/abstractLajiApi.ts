import { RESTDataSource } from 'apollo-datasource-rest';
import { RequestInit, URLSearchParamsInit } from 'apollo-server-env';

const PARAM_PAGE = process.env.PARAM_PAGE || 'page';
const PROPERTY_LAST_PAGE = process.env.PROPERTY_LAST_PAGE || 'lastPage';
const PROPERTY_RESULTS = process.env.PROPERTY_RESULTS || 'results';

export abstract class AbstractLajiApi extends RESTDataSource {

  get baseURL(): string {
    if (process.env.lajiApi) {
      return process.env.lajiApi;
    }
    if (this.context.env === 'production') {
      return 'https://api.laji.fi/v0/';
    } else {
      return 'http://localhost:3003/v0/';
    }
  }

  protected get<TResult = any>(path: string, params?: URLSearchParamsInit, init?: RequestInit): Promise<TResult> {
    return super.get(path, params, {
      headers: {
        'Authorization': this.context.accessToken,
        'Content-Type': 'application/json',
      },
      timeout: this.context.timeout,
      compress: true,
    });
  }

  protected async getAllPaged<TResult>(path: string, params?: { [key: string]: Object | Object[] | undefined }): Promise<TResult[]> {
    const result = await this.get<any>(path, params);
    const last = result[PROPERTY_LAST_PAGE] || 1;
    const response: TResult[] = [...result[PROPERTY_RESULTS]];

    for(let i = 2; i <= last; i++) {
      const pageResponse = await this.get(path, {...params, [PARAM_PAGE]: i});
      response.push(...pageResponse[PROPERTY_RESULTS]);
    }

    return response;
  }

  protected transformResult(item: any) {
    return item;
  }
}
