import { AbstractLajiApi } from './abstractLajiApi';

export abstract class AbstractLajiApiGetters extends AbstractLajiApi {
  protected abstract basePath: string;

  getAll({id, ...args}: any) {
    return id ?
      this.get(`${this.basePath}/${id}`, args).then(item => [this.transformResult(item)]) :
      this.get(this.basePath, args)
        .then(result => result.results || result)
        .then(result => Array.isArray(result) ? result.map(item => this.transformResult(item)) : this.transformResult(result));
  }
}
