import { AbstractLajiApi } from './abstractLajiApi';

export abstract class AbstractLajiApiPagedGetters extends AbstractLajiApi {
  protected abstract basePath: string;

  getAll({id, ...args}: any) {
    return id ?
      this.get(`${this.basePath}/${id}`, args)
        .then(item => ({currentPage: 1, lastPage: 1, pageSize: 1, total: 1, results: [this.transformResult(item)]})) :
      this.get(this.basePath, args)
        .then(result => ({
          ...result,
          results: result.results.map((item: any) => this.transformResult(item))
        }));
  }
}
