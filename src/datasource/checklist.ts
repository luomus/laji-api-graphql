import { AbstractLajiApiGetters } from './abstractLajiApiGetters';

export class Checklist extends AbstractLajiApiGetters {
  protected basePath: string = '/checklists';

  protected transformResult(item: any): any {
    return {
      ...item,
      bibliographicCitation: item['dc:bibliographicCitation']
    };
  }
}
