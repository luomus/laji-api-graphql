import { AbstractLajiApiGetters } from './abstractLajiApiGetters';

export class ChecklistVersion extends AbstractLajiApiGetters {
  protected basePath: string = '/checklistVersions';
}
