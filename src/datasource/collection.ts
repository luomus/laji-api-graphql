import { AbstractLajiApiGetters } from './abstractLajiApiGetters';
import DataLoader from 'dataloader';

export class Collection extends AbstractLajiApiGetters {
  protected basePath: string = '/collections';
  protected collections: any[]|null = null;

  getAll({id, ...args}: any) {
    if (id) {
      return this.get(`${this.basePath}/${id}`, args).then(item => [item]);
    }
    return this.get(`${this.basePath}/roots`, args).then(result => result.results);
  }

  async getChildren({id, ...args}: any) {
    return (await this.childLoader(args)).load(id);
  }

  protected async childLoader(args: any) {
    if (!this.collections) {
      this.collections = await this.getAllPaged(`${this.basePath}`, {...args, pageSize: 1000});
    }
    return new DataLoader((keys) => {
      const result = keys.map((collectionId) => {
        return (this.collections || []).filter(collection => collection.isPartOf === collectionId)
      })

      return Promise.resolve(result)
    });
  }
}
