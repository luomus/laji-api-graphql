import { AbstractLajiApiPagedGetters } from './abstractLajiApiPagedGetters';

export class Document extends AbstractLajiApiPagedGetters {
  protected basePath: string = '/documents';
}
