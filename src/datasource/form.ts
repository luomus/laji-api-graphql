import { AbstractLajiApiGetters } from './abstractLajiApiGetters';
import { FieldNode } from 'graphql/language/ast';
import { isFieldSelected } from '../utils/QueryHelper';

export class Form extends AbstractLajiApiGetters {
  protected basePath: string = '/forms';

  getById(args: any, fieldNodes: ReadonlyArray<FieldNode> = []): Promise<any> {
    const useFieldType = isFieldSelected(fieldNodes, 'fields');
    const format = args.format || (useFieldType ? 'json' : undefined)

    const _args = {...args};
    if (format) {
      _args.format = format;
    }
    return super.getAll(_args).then(results => results[0]);

  }
}
