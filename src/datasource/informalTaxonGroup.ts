import { AbstractLajiApiGetters } from './abstractLajiApiGetters';

export class InformalTaxonGroup extends AbstractLajiApiGetters {
  protected basePath: string = '/informal-taxon-groups';

  getAll({id, ...args}: any) {
    if (id) {
      return this.get(`${this.basePath}/${id}`, args).then(item => [item]);
    }
    return this.get(`${this.basePath}/roots`, args).then(result => result.results);
  }

  getChildren({id, ...args}: any) {
    return this.getAllPaged(`${this.basePath}/${id}/children`, args);
  }

  getParents({id, ...args}: any) {
    return this.get(`${this.basePath}/${id}/parents`, args)
      .catch(() => []);
  }
}
