import { AbstractLajiApiGetters } from './abstractLajiApiGetters';

export class Information extends AbstractLajiApiGetters {
  protected basePath: string = '/information';

  getAll(args: any): Promise<any[]> | Promise<any> {
    return super.getAll(args).then(data => Array.isArray(data) ? data[0] : data);
  }
}
