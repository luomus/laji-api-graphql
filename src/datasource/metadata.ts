import { AbstractLajiApi } from './abstractLajiApi';

export class Metadata extends AbstractLajiApi {

  getAllClasses({id, ...args}: any) {
    return this.get('metadata/classes', args)
      .then((result: any) => result.results)
      .then(classes => id ? classes.filter((item: any) => item.class === id) : classes);
  }

  getAllProperties({id, ...args}: any) {
    return this.get('metadata/properties', args)
      .then((result: any) => result.results.map((prop: any) => ({...prop, label: prop.label || prop.shortName || prop.property})))
      .then(properties => properties.map((prop: any) => this.prepareProperties(prop)))
      .then(properties => id ? properties.filter((prop: any) => prop.property === id) : properties);
  }

  getAllRanges({id, ...args}: any) {
    return this.get('metadata/ranges', args)
      .then((result: any) => Object.keys(result).map((alt) => ({
        alt,
        options: this.prepareOptions(result[alt])
      })))
      .then(alts => id ? alts.filter((alt: any) => alt.alt === id) : alts);
  }

  getWarehouseLabels({id, ...args}: any) {
    return this.get('warehouse/enumeration-labels', args)
      .then((result: any) => result.results.filter((prop: any) => !!prop.enumeration))
      .then((enumerations => id ? enumerations.filter((enumeration: any) => enumeration.enumeration === id) : enumerations));
  }

  private prepareProperties(properties: any) {
    return {
      ...properties,
      multiLang: properties.multiLanguage,
      embedded: properties.isEmbeddable,
    }
  }

  private prepareOptions(options: {
    id: string,
    value: string,
    administrativeStatusDescription: string,
    administrativeStatusLink: string,
    description: string,
    link: string
  }[]) {
    return options.map(option => ({
      id: option.id,
      value: option.value || option.id,
      description: option.administrativeStatusDescription || option.description,
      link: option.administrativeStatusLink || option.link
    }));
  }
}
