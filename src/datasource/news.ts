import { AbstractLajiApiPagedGetters } from './abstractLajiApiPagedGetters';

export class News extends AbstractLajiApiPagedGetters {
  protected basePath: string = '/news';
}
