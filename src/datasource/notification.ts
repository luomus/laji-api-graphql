import { AbstractLajiApiPagedGetters } from './abstractLajiApiPagedGetters';

export class Notification extends AbstractLajiApiPagedGetters {
  protected basePath: string = '/notifications';
}
