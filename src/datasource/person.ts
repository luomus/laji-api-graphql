import { AbstractLajiApi } from './abstractLajiApi';

export class Person extends AbstractLajiApi {

  getAll({personToken, id, ...args}: any) {
    if (personToken) {
      return this.get(`/person/${personToken}`, args)
        .then(person => id ? (person.id === id ? [person] : []) : [person])
        .catch(() => []);
    }
    if (id) {
      return this.get(`/person/by-id/${id}`, args)
        .then(person => [person])
        .catch(() => []);
    }
  }
}
