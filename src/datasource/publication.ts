import { AbstractLajiApiPagedGetters } from './abstractLajiApiPagedGetters';

export class Publication extends AbstractLajiApiPagedGetters {
  protected basePath: string = '/publications';


  protected transformResult(item: any): any {
    return {
      ...item,
      URI: item['dc:URI'],
      bibliographicCitation: item['dc:bibliographicCitation'],
    };
  }
}
