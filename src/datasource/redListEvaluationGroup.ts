import { InformalTaxonGroup } from './informalTaxonGroup';

export class RedListEvaluationGroup extends InformalTaxonGroup {
  protected basePath: string = '/red-list-evaluation-groups';
}
