import { AbstractLajiApiGetters } from './abstractLajiApiGetters';
import { FieldNode } from 'graphql/language/ast';
import { extractFieldsFromFieldSet, getSelectionSet } from '../utils/QueryHelper';

export class Taxon extends AbstractLajiApiGetters {
  protected basePath: string = '/taxa';

  getAll(args: any, fieldNodes: ReadonlyArray<FieldNode> = []) {
    return super.getAll(this.argsFromFieldNodes(args, fieldNodes));
  }

  getChildren({id, ...args}: any, fieldNodes: ReadonlyArray<FieldNode> = []) {
    return this.get(`${this.basePath}/${id}/children`, this.argsFromFieldNodes(args, fieldNodes))
      .then(result => result.map((item: any) => this.transformResult(item)));
  }

  getSpecies({id, ...args}: any, fieldNodes: ReadonlyArray<FieldNode> = []) {
    return this.get(`${this.basePath}/${id}/species`, this.argsFromFieldNodes(args, fieldNodes))
      .then(result => result.results.map((item: any) => this.transformResult(item)));
  }

  getById(args: any, fieldNodes: ReadonlyArray<FieldNode> = []): Promise<any> {
    return this.getAll(args, fieldNodes).then(results => results[0]);
  }

  private argsFromFieldNodes(args: any, fieldNodes: ReadonlyArray<FieldNode>) {
    return {
    ...args,
      selectedFields: extractFieldsFromFieldSet(getSelectionSet(fieldNodes, 'taxon')).join(',')
    }
  }

  protected transformResult(item: any): any {
    return {
      ...item,
      isSpecies: item['species'],
      isFinnishSpecies: item['finnishSpecies']
    }
  }
}
