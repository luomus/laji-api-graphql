import { AbstractLajiApi } from './abstractLajiApi';
import { FieldNode } from 'graphql/language/ast';
import { dotValueObjToObj, extractFieldsFromFieldSet } from '../utils/QueryHelper';

export class Unit extends AbstractLajiApi {
  protected basePath: string = '/warehouse';

  getList({aggregateBy, ...args}: any, fieldNodes: ReadonlyArray<FieldNode>) {
    const useCount = this.useCount(fieldNodes);
    if (aggregateBy) {
      return this.getAggregatedList(args, aggregateBy, useCount, fieldNodes);
    }
    return useCount ?
      this.get(`${this.basePath}/query/count`, args) :
      this.get(`${this.basePath}/query/list`, {
        ...args,
        selected: extractFieldsFromFieldSet(this.getResultSelectionSet(fieldNodes), {'count': ''}).join(',')
      });
  }

  private getAggregatedList(args: any, aggregateBy: string, useCount: boolean, fieldNodes: ReadonlyArray<FieldNode>) {
    return useCount ?
      this.get(`${this.basePath}/query/aggregate`, {...args, aggregateBy, pageSize: 1, page: 1}) :
      this.get(`${this.basePath}/query/aggregate`, {...args, aggregateBy: this.getAggregatedBy(aggregateBy, fieldNodes)})
        .then((result: any) => {
          result.results = result.results.map((aggr: any) => dotValueObjToObj(aggr['aggregateBy'], {count: aggr['count']}));
          return result;
        });
  }

  private getResultSelectionSet(fieldNodes: ReadonlyArray<FieldNode>) {
    let result = {selections: []};
    fieldNodes.forEach(set => {
      if (!set.selectionSet) {
        return
      }
      set.selectionSet.selections.forEach((selection: any) => {
        if (selection.name && selection.name.value === 'results' && selection.selectionSet) {
          result = selection.selectionSet;
        }
      });
    });
    return result;
  }

  private useCount(fieldNodes: ReadonlyArray<FieldNode>): boolean {
    let result = true;
    fieldNodes.forEach(set => {
      if (set.selectionSet && set.selectionSet.selections.some((selection: any) => selection.name.value !== 'total')) {
        result = false;
      }
    });
    return result;
  }

  private getAggregatedBy(aggregateBy: string, fieldNodes: ReadonlyArray<FieldNode>): string {
    const resultSet = this.getResultSelectionSet(fieldNodes);
    const fields = extractFieldsFromFieldSet(resultSet, {'count': ''});

    fields.forEach(field => {
      const pattern = `\b${field.replace('.', '\.')}\b`;
      const re = new RegExp(pattern);
      if (!re.test(aggregateBy)) {
        aggregateBy += ',' + field;
      }
    });

    return aggregateBy;
  }
}
