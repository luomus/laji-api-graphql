import { join } from 'path';
import * as winston from 'winston';

const logPath = join(__dirname, '..', '..', 'logs');
let currentDate: string;
let logger: winston.Logger;

function getLogger(): winston.Logger {
  const now = new Date();
  const today = now.toISOString().substring(0, 10) + '.log';
  if (currentDate !== today) {
    currentDate = today;
    logger = winston.createLogger({
      transports: [
        new (winston.transports.File)({ filename: join(logPath, currentDate)})
      ]
    });
  }
  return logger;
}


export function logging() {
  return {
    requestDidStart(requestContext: any) {
      getLogger().info({
        message: 'QUERY',
        query: requestContext.request.query,
        variables: requestContext.request.variables,
        accessToken: requestContext.context.accessToken
      });
    }
  }
}
