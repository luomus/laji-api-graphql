import { IRelation } from '../utils';

const Area: IRelation = {
  isPartOf: {
    fragment: 'fragment isPartOf on Area { isPartOf }',
    resolve(parent, {id, type, ...args}, {dataSources, lang}) {
      return dataSources.area.getAll({...args, id: parent.isPartOf, lang})
        .then((areas: any[]) => areas.pop());
    }
  }
};

export { Area as default }
