import { IRelation } from '../utils';

const Checklist: IRelation = {
  species: {
    fragment: 'fragment species on Checklist { id }',
    resolve(parent, {id, ...args}, {dataSources, lang}, info) {
      return dataSources.taxon.getAll({...args, species: true, checklist: parent.id, lang});
    }
  },
  taxon: {
    fragment: 'fragment taxa on Checklist { rootTaxon }',
    resolve(parent, {id, ...args}, {dataSources, lang}, info) {
      return dataSources.taxon.getAll({...args, id: parent.rootTaxon, lang}).then(results => results.pop());
    }
  }
};

export { Checklist as default }
