import { IRelation } from '../utils';

const Collection: IRelation = {
  children: {
    fragment: 'fragment children on Collection { id }',
    resolve(parent, args, {dataSources, lang}, info) {
      return dataSources.collection.getChildren({...args, id: parent.id, lang});
    }
  },
};

export { Collection as default }
