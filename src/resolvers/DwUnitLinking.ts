import { IRelation } from '../utils';
import { toQname } from '../utils/QueryHelper';

const DwUnitLinking: IRelation = {
  taxon: {
    fragment: 'fragment taxon on DwUnitLinking { id }',
    resolve(parent, {id, type, ...args}, {dataSources, lang}) {
      const taxonId = parent && parent.taxon && parent.taxon.id || null;
      if (!taxonId) {
        return null;
      }
      return dataSources.taxon.getAll({...args, id: toQname(taxonId), lang})
        .then((taxa: any[]) => taxa.pop());
    }
  }
};

export { DwUnitLinking as default }
