import { IRelation } from '../utils';

const InformTaxonGroup: IRelation = {
  parent: {
    fragment: 'fragment parent on InformTaxonGroup { id }',
    resolve(parent, args, {dataSources, lang}, info) {
      return dataSources.informalTaxonGroup.getParents({...args, id: parent.id, lang}).then((parents: any[]) => parents.pop());
    }
  },
  children: {
    fragment: 'fragment children on InformTaxonGroup { id }',
    resolve(parent, args, {dataSources, lang}, info) {
      return dataSources.informalTaxonGroup.getChildren({...args, id: parent.id, lang});
    }
  },
};

export { InformTaxonGroup as default }
