import { IRelation } from '../utils';

const Metadata: IRelation = {
  properties: {
    fragment: 'fragment properties on Metadata { domain }',
    resolve(parent, {id, ...args}, {dataSources}, info) {
      return dataSources.metadata.getAllProperties(args)
        .then(properties => properties.filter((property: any) => property.domain.includes(parent.class)))
    }
  }
};

export { Metadata as default }
