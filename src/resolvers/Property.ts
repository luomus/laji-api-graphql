import { IRelation } from '../utils';

const Property: IRelation = {
  alts: {
    fragment: 'fragment alts on Property { range }',
    resolve(parent, {id, ...args}, {dataSources}, info) {
      return dataSources.metadata.getAllRanges(args)
        .then(alts => alts.filter((alt: any) => parent.range.includes(alt.alt)));
    }
  },
  classes: {
    fragment: 'fragment classes on Property { range }',
    resolve(parent, {id, ...args}, {dataSources}, info) {
      return dataSources.metadata.getAllClasses(args)
        .then(classes => classes.filter((meta: any) => parent.range.includes(meta.class)));
    }
  },
};

export { Property as default }
