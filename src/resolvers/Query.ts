import { IFieldResolver } from 'graphql-tools';
import { IContext } from '../utils';
import { transformArgs } from '../utils/QueryHelper';

interface IQuery {
  [key: string]: IFieldResolver<any, IContext>;
}

export const Query: IQuery = {
  status(source, args): string {
    return `OK`;
  },
  alts: (source, args, {dataSources, lang}) => {
    return dataSources.metadata.getAllRanges(transformArgs(args, {pageSize: 1000, lang}));
  },
  areas: (source, args, {dataSources, lang}) => {
    return dataSources.area.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  checklists: (source, args, {dataSources, lang}) => {
    return dataSources.checklist.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  collection: (source, args, {dataSources, lang}) => {
    return dataSources.collection.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  checklistVersions: (source, args, {dataSources, lang}) => {
    return dataSources.checklistVersion.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  classes: (source, args, {dataSources, lang}) => {
    return dataSources.metadata.getAllClasses(transformArgs(args, {pageSize: 100, lang}));
  },
  forms: (source, args, {dataSources, lang}) => {
    return dataSources.form.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  form: (source, args, {dataSources, lang}, {fieldNodes}) => {
    return dataSources.form.getById(transformArgs(args, {pageSize: 1000, lang}), fieldNodes);
  },
  documents: (source, args, {dataSources, lang}) => {
    return dataSources.document.getAll(transformArgs(args, {pageSize: 100, format: 'schema', lang}));
  },
  informalTaxonGroups: (source, args, {dataSources, lang}) => {
    return dataSources.informalTaxonGroup.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  news: (source, args, {dataSources, lang}) => {
    return dataSources.news.getAll(transformArgs(args, {pageSize: 10, lang}));
  },
  notifications: (source, args, {dataSources, lang}) => {
    return dataSources.notification.getAll(transformArgs(args, {pageSize: 10, lang}));
  },
  information: (source, args, {dataSources, lang}) => {
    return dataSources.information.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  person: (source, args, {dataSources, lang}) => {
    return dataSources.person.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  properties: (source, args, {dataSources, lang}) => {
    return dataSources.metadata.getAllProperties(transformArgs(args, {pageSize: 1000, lang}));
  },
  warehouseLabels: (source, args, {dataSources, lang}) => {
    return dataSources.metadata.getWarehouseLabels(transformArgs(args, {pageSize: 1000, lang}));
  },
  publications: (source, args, {dataSources, lang}) => {
    return dataSources.publication.getAll(transformArgs(args, {pageSize: 10, lang}));
  },
  redListEvaluationGroup: (source, args, {dataSources, lang}) => {
    return dataSources.redListEvaluationGroup.getAll(transformArgs(args, {pageSize: 1000, lang}));
  },
  taxa: (source, args, {dataSources, lang}, {fieldNodes}) => {
    return dataSources.taxon.getAll(transformArgs(args, {pageSize: 1000, lang}), fieldNodes);
  },
  taxon: (source, args, {dataSources, lang}, {fieldNodes}) => {
    return dataSources.taxon.getById(transformArgs(args, {lang}), fieldNodes);
  },
  units: (source, args, {dataSources, lang}, {fieldNodes}) => {
    return dataSources.unit.getList(transformArgs(args, {pageSize: 100}), fieldNodes);
  }
};
