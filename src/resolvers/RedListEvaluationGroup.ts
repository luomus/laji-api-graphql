import { IRelation } from '../utils';

const RedListEvaluationGroup: IRelation = {
  parent: {
    fragment: 'fragment parent on RedListEvaluationGroup { id }',
    resolve(parent, args, {dataSources}, info) {
      return dataSources.redListEvaluationGroup.getParents({...args, id: parent.id}).then((parents: any[]) => parents.pop());
    }
  },
  children: {
    fragment: 'fragment children on RedListEvaluationGroup { id }',
    resolve(parent, args, {dataSources}, info) {
      return dataSources.redListEvaluationGroup.getChildren({...args, id: parent.id});
    }
  },
};

export { RedListEvaluationGroup as default }
