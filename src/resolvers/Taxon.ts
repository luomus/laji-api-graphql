import { IRelation } from '../utils';

const Taxon: IRelation = {
  children: {
    fragment: 'fragment children on Taxon { id }',
    resolve(parent, {id, type, ...args}, {dataSources, lang}, info) {
      return dataSources.taxon.getChildren({...args, id: parent.id, lang});
    }
  },
  species: {
    fragment: 'fragment species on Taxon { id }',
    resolve(parent, {id, type, ...args}, {dataSources, lang}, info) {
      return dataSources.taxon.getSpecies({...args, id: parent.id, lang});
    }
  }
};

export { Taxon as default }
