import { IResolvers } from 'graphql-tools';
import { IContext } from '../utils';
import { Query } from './Query';
import Area from './Area';
import Checklist from './Checklist';
import Collection from './Collection';
import Metadata from './Metadata';
import Property from './Property';
import InformalTaxonGroup from './InformalTaxonGroup';
import RedListEvaluationGroup from './RedListEvaluationGroup';
import Taxon from './Taxon';
import GraphQLJSON, { GraphQLJSONObject } from 'graphql-type-json';

export default {
  Query,
  Area,
  Collection,
  Checklist,
  InformalTaxonGroup,
  Metadata,
  Property,
  RedListEvaluationGroup,
  Taxon,
  JSON: GraphQLJSON,
  JSONObject: GraphQLJSONObject
} as IResolvers<any, IContext>
