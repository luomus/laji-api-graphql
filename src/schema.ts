import 'graphql-import-node';
import * as typeDefs from './schema/laji.graphql';
import { makeExecutableSchema } from 'graphql-tools';
import { GraphQLSchema } from 'graphql';
import resolvers from './resolvers'
import {IContext} from './utils';

const schema: GraphQLSchema = makeExecutableSchema<IContext>({
    typeDefs,
    resolvers,
});
export default schema;
