import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import responseCachePlugin from 'apollo-server-plugin-response-cache';
import { GraphQLRequestContext } from 'apollo-server-types';
import depthLimit from 'graphql-depth-limit';
import { createServer } from 'http';
import compression from 'compression';
import cors from 'cors';

import schema from './schema';
import { getLang, getTimeout } from './utils/QueryHelper';
import { Metadata } from './datasource/metadata';
import { Information } from './datasource/information';
import { InformalTaxonGroup } from './datasource/informalTaxonGroup';
import { Person } from './datasource/person';
import { Area } from './datasource/area';
import { Checklist } from './datasource/checklist';
import { ChecklistVersion } from './datasource/checklistVersion';
import { Document } from './datasource/document';
import { Form } from './datasource/form';
import { News } from './datasource/news';
import { RedListEvaluationGroup } from './datasource/redListEvaluationGroup';
import { Taxon } from './datasource/taxon';
import { Unit } from './datasource/unit';
import { Notification } from './datasource/notification';
import { Publication } from './datasource/publication';
import { RedisCache } from 'apollo-server-cache-redis';
import { logging } from './plugins/logging';
import { Collection } from './datasource/collection';

const app = express();
const server = new ApolloServer({
  schema,
  validationRules: [depthLimit(7)],
  cache: new RedisCache({
    host: process.env.REDIS_HOST || 'localhost'
  }),
  cacheControl: {
    defaultMaxAge: 30
  },
  plugins: [
    responseCachePlugin({
      extraCacheKeyData(requestContext: GraphQLRequestContext<Record<string, any>>): Promise<any> | any {
        if (requestContext.request.http) {
          return [
            requestContext.request.http.headers.get('Authorization'),
            getLang(requestContext.request.http.headers.get('Accept-Language'))
          ].join(':')
        }
        return 'en';
      }
    }),
    // logging()
  ],
  dataSources: () => {
    return {
      area: new Area(),
      checklist: new Checklist(),
      checklistVersion: new ChecklistVersion(),
      collection: new Collection(),
      document: new Document(),
      form: new Form(),
      information: new Information(),
      informalTaxonGroup: new InformalTaxonGroup(),
      unit: new Unit(),
      person: new Person(),
      publication: new Publication(),
      metadata: new Metadata(),
      news: new News(),
      notification: new Notification(),
      redListEvaluationGroup: new RedListEvaluationGroup(),
      taxon: new Taxon(),
    };
  },
  context: ({ req }) => {
    return {
      env: process.env.NODE_ENV,
      lang: getLang(req.headers['accept-language']),
      accessToken: req.headers['authorization'],
      timeout: getTimeout(req.headers['x-timeout'])
    };
  },
});
app.use('*', cors());
app.use(compression());
server.applyMiddleware({app, path: '/v0/graphql'});

const httpServer = createServer(app);
const port = process.env.PORT || 3000;

httpServer.listen(
  {port: port},
  (): void => console.log(`GraphQL is now running on http://localhost:${port}/v0/graphql`)
);
