import { IResolverOptions } from 'graphql-tools';
import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';
import { Area } from './datasource/area';
import { Metadata } from './datasource/metadata';
import { News } from './datasource/news';
import { Checklist } from './datasource/checklist';
import { ChecklistVersion } from './datasource/checklistVersion';
import { Document } from './datasource/document';
import { InformalTaxonGroup } from './datasource/informalTaxonGroup';
import { Information } from './datasource/information';
import { Unit } from './datasource/unit';
import { Person } from './datasource/person';
import { RedListEvaluationGroup } from './datasource/redListEvaluationGroup';
import { Taxon } from './datasource/taxon';
import { Form } from './datasource/form';
import { Notification } from './datasource/notification';
import { Publication } from './datasource/publication';
import { Collection } from './datasource/collection';

export interface IContext extends ExpressContext {
  dataSources: IDateSource
  env?: 'production' | 'development',
  lang: 'fi' | 'en' | 'sv',
  accessToken?: string;
}

export interface IRelation {
  [key: string]: IResolverOptions<any, IContext>;
}

export interface IDateSource {
  area: Area;
  collection: Collection;
  checklist: Checklist;
  checklistVersion: ChecklistVersion;
  document: Document;
  form: Form;
  metadata: Metadata;
  news: News;
  notification: Notification;
  information: Information;
  informalTaxonGroup: InformalTaxonGroup;
  person: Person;
  publication: Publication;
  redListEvaluationGroup: RedListEvaluationGroup;
  taxon: Taxon;
  unit: Unit;
}
