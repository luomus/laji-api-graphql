import { FieldNode, SelectionNode } from 'graphql/language/ast';

function getSelectionSet(fieldNodes: ReadonlyArray<FieldNode>, fieldName: string): ReadonlyArray<SelectionNode> {
  for(const node of fieldNodes) {
    if (node.name && node.name.value === fieldName) {
      return node.selectionSet && node.selectionSet.selections || [];
    }
  }
  return [];
}

function extractFieldsFromFieldSet(set: any, replace: {[key: string]: string} = {}, result: string[] = [], parent = '') {
  const col = set && set.selections || set || [];
  col.forEach((selection: any) => {
    const path = parent ? `${parent}.${selection.name.value}` : selection.name.value;
    if (typeof replace[path] !== 'undefined') {
      if (replace[path]) {
        result.push(replace[path]);
      }
      return;
    }
    selection.selectionSet ? extractFieldsFromFieldSet(selection.selectionSet, replace, result, path) : result.push(path);
  });
  return result.filter(item => item !== 'count');
}

function toQname(id: string): string {
  if (!id) {
    return id;
  }
  return id.replace('http://tun.fi/', '');
}

function isFieldSelected(fieldNodes: ReadonlyArray<FieldNode>, field: string): boolean {
  return fieldNodes
    .some(set => set.selectionSet && set.selectionSet.selections.some((selection: any) => selection.name.value === field))
}

function dotValueObjToObj(value: {[key: string]: string}, base = {}) {
  if (!value) {
    return base;
  }
  Object.keys(value).forEach(path => {
    const parts = path.split('.');
    const last = parts.length - 1;
    let idx = 0;
    let pointer: any = base;
    while(!!parts[idx]) {
      const part = parts[idx];
      if (idx === last) {
        pointer[part] = value[path];
        break;
      } else if (!pointer[part]) {
        pointer[part] = {}
      }
      pointer = pointer[part];
      idx++;
    }
  });
  return base;
}

function transformArgs(args: any, base: any) {
  return {
    ...base,
    ...args
  }
}

function getLang(lang: any, defaultLang = 'en', knownLang = ['fi', 'en', 'sv']) {
  if (!lang || typeof lang !== 'string') {
    return defaultLang;
  }
  lang = lang.substr(0, 2);
  return knownLang.includes(lang) ? lang : defaultLang;
}

function getTimeout(timeout: any) {
  if (timeout) {
    const num = Number(timeout);
    if (num >= 10 || num <= 120) {
      return num * 1000;
    }
  }
  return 10000;
}

export {
  getSelectionSet,
  extractFieldsFromFieldSet,
  toQname,
  dotValueObjToObj,
  getLang,
  getTimeout,
  transformArgs,
  isFieldSelected
}
